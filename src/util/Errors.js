import { Errors as BaseErrors } from 'form-backend-validation'

export default class Errors extends BaseErrors {
  /**
   * Create a new Errors instance.
   */
  constructor(message = null, errors = {}) {
    super(errors)
    this.message = message
  }

  setMessage(message) {
    this.message = message
  }

  getMessage() {
    return this.message
  }

  anyMessage() {
    return this.message != null
  }

  any() {
    return super.any() || this.anyMessage()
  }

  clearMessage() {
    this.message = null
  }

  clearAll() {
    this.clear()
    this.clearMessage()
  }
}
