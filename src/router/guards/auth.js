import _ from 'lodash'

export default {
  checkIfAuthenticated: (to, from, next) => {
    const modules = { app: null, user: null }

    _.forEach(modules, (value, key) => {
      modules[key] = JSON.parse(window.localStorage.getItem(key))
    })

    if (modules.app != null && modules.user != null) {
      return next({ name: 'search' })
    }

    next()
  },
  authenticate: (to, from, next) => {
    const modules = { app: null, user: null }

    _.forEach(modules, (value, key) => {
      modules[key] = JSON.parse(window.localStorage.getItem(key))
    })

    if (modules.app == null || modules.user == null) {
      return next({ name: 'home' })
    }

    next()
  }
}
