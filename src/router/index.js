import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from '../components/Layouts/Auth.vue'
import Dashboard from '../components/Layouts/Dashboard.vue'
import { auth as authGuard } from '@/router/guards'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Auth,
    beforeEnter: authGuard.checkIfAuthenticated,
    children: [
      {
        path: '',
        name: 'home',
        component: () =>
          import(/* webpackChunkName: "home" */ '../views/Home.vue')
      }
    ]
  },
  {
    path: '/search',
    component: Dashboard,
    beforeEnter: authGuard.authenticate,
    children: [
      {
        path: '',
        name: 'search',
        component: () =>
          import(/* webpackChunkName: "search" */ '../views/Search.vue')
      }
    ]
  },
  {
    path: '*',
    redirect: { name: 'home' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
