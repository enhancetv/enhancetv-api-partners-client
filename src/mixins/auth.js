import { mapGetters, mapMutations } from 'vuex'

export default {
  computed: {
    ...mapGetters('auth', ['auth'])
  },
  methods: {
    ...mapMutations('auth', ['updateApp', 'updateUser']),
    subscribe(type, callback) {
      return this.$store.subscribe(mutation => {
        if (mutation.type === type) {
          callback()
        }
      })
    },
    subscribeToApp(callback) {
      return this.subscribe('auth/updateApp', callback)
    },
    subscribeToUser(callback) {
      return this.subscribe('auth/updateUser', callback)
    }
  }
}
