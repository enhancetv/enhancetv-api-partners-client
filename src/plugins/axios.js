import Vue from 'vue'
import axios from 'axios'

const instance = axios.create()

instance.interceptors.response.use(
  response => {
    return response
  },
  error => {
    const { status } = error.response

    if (status === 401) {
      window.localStorage.removeItem('app')
      window.localStorage.removeItem('user')

      window.location.href = '/'
    }

    return Promise.reject(error)
  }
)

Vue.prototype.$axios = instance
