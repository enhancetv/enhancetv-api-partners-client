import '@babel/polyfill'
import 'mutationobserver-shim'

import './plugins'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { auth as authMixin } from '@/mixins'

Vue.config.productionTip = false

Vue.mixin(authMixin)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
