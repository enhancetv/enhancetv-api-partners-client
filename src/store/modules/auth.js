export default {
  namespaced: true,
  state: () => ({
    auth: {
      app: {},
      user: {}
    }
  }),
  getters: {
    auth: state => state.auth
  },
  mutations: {
    updateApp(state, app) {
      state.auth.app = app
    },
    updateUser(state, user) {
      state.auth.user = user
    }
  }
}
