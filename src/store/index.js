import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import auth from './modules/auth'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

function persistedAuth() {
  const modules = { app: 'auth/updateApp', user: 'auth/updateUser' }

  return store => {
    // called when the store is initialized
    _.forEach(modules, (type, key) => {
      const payload = JSON.parse(window.localStorage.getItem(key))

      if (payload) {
        store.commit(type, payload)
      }
    })

    store.subscribe(mutation => {
      // called after every mutation.
      // The mutation comes in the format of `{ type, payload }`.
      _.forEach(modules, (type, key) => {
        if (mutation.type === type) {
          const value = JSON.stringify(mutation.payload)

          window.localStorage.setItem(key, value)
        }
      })
    })
  }
}

export default new Vuex.Store({
  strict: debug,
  modules: { auth },
  plugins: [persistedAuth()]
})
